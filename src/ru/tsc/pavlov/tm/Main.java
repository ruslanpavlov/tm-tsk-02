package ru.tsc.pavlov.tm;

import ru.tsc.pavlov.tm.constant.TerminalConst;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        showWelcome();
        parseArgs(args);
    }

    public static void parseArgs(String[] args){
        if (args == null || args.length == 0) return;
        final String arg = args [0];
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
        if (TerminalConst.HELP.equals(arg)) showHelp();
    }

    public static void showWelcome(){
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public static void showAbout(){
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Ruslan Pavlov");
        System.out.println("E-MAIL: o000.ruslan@yandex.ru");
        System.exit(0);
    }

    public static void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
        System.exit(0);
    }

    public static void showHelp(){
        System.out.println("[HELP]");
        System.out.println("about - display developer info");
        System.out.println("version - display program info");
        System.out.println("help - display list of commands");
        System.exit(0);
    }
}
